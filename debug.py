import inspect
import re


def p(x): return print(f"{x} | {type(x)}")

# https://izziswift.com/how-can-you-print-a-variable-name-in-python/
def vn(p):
    for line in inspect.getframeinfo(inspect.currentframe().f_back)[3]:
        m = re.search(r'\bvn\s*\(\s*([A-Za-z_][A-Za-z0-9_]*)\s*\)', line)
        if m:
            return m.group(1)

def pr(v):
    for line in inspect.getframeinfo(inspect.currentframe().f_back)[3]:
        m = re.search(r'\bpr\s*\(\s*([A-Za-z_][A-Za-z0-9_]*)\s*\)', line)
        if(m):
            return print(f"{m.group(1)} = {v} | {type(v)}")

if(__name__ == "__main__"):
    eggs = 5
    pr(eggs)
