import csv
import os
from parseGML import *
import debug as d

import os.path

"""
TODO:
    - CSV {Name of Graph, Num of Nodes, Num of Edges, Average Edges for all the Nodes }
"""


def csvFromDIR(DIR, name):

    dct = dict()

    for filename in os.scandir(DIR):
        filename = filename.path

        # Fixes GML to be a standard notation
        fixGML(filename)

        # Read the gml file into the Graph Object within Networkx
        G = nx.read_gml(filename)

        # Dictionary of Filenames and Graph Objects
        dct[filename] = G

    # CSV generation happens
    with open(name, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=",")

        for key in dct:
            g = dct[key]

            num_nodes = num_of_nodes(g)

            num_edges = num_of_edges(g)

            # Average Edges for all the Nodes
        
            writer.writerow([key, num_nodes, num_edges])


def generateStatsFromPreMLmalwareData(repoDIR, name):

    # Initalize Data Structure
    dct = dict()

    count = 0

    DIR = f"{repoDIR}/FinishedData"

    for filename in os.scandir(DIR):
        file = filename.path
        hash = file.split("/")[-1][13:]
        file = f"{file}/ControlFlowGraph.gml"

        if(os.path.exists(file)):

            count += 1
            if(count % 100 == 0):
                print(count)
            
            print(hash)

            fixGML(file)

            G = nx.read_gml(file,label='id')

            num_nodes = num_of_nodes(G)

            num_edges = num_of_edges(G)

            dct[hash] = {'Graph': G, 'num_of_nodes': num_nodes, 'num_of_edges': num_edges}

    print(f"Total count: {count}")

    with open(name, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=",")

        for key in dct:
            writer.writerow([key, dct[key]['num_of_nodes'], dct[key]['num_of_edges']])



if __name__ == "__main__":


    #Put in directory
    #Go through 1.gml-5.gml
    # main("test", "1.csv")

    generateStatsFromPreMLmalwareData('C:/Users/Andres/Documents/projects/preMLmalwareData','output.csv')
    


    pass