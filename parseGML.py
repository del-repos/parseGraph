#!/usr/bin/env python
import debug as d
import networkx as nx


"""
TODO:
    - Get num of edges per node
"""


# Fixes GML file by changing True, 0 -> "True" and the same pattern for False
def fixGML(filename):
    retFile = ""
    with open(filename, 'r+') as f:
        file = f.read()

        # Fix External 
        retFile = file.replace('external True', 'external "True"')
        retFile = retFile.replace('external False', 'external "False"')
        retFile = retFile.replace('external 1', 'external "True"')
        retFile = retFile.replace('external 0', 'external "False"')        

        # Fix Entrypoint
        retFile = retFile.replace('entrypoint True', 'entrypoint "True"')
        retFile = retFile.replace('entrypoint False', 'entrypoint "False"')
        retFile = retFile.replace('entrypoint 1', 'entrypoint "True"')
        retFile = retFile.replace('entrypoint 0', 'entrypoint "False"')

    # Rewrites the file with the replaced text
    with open(filename, 'w') as f:
        f.write(retFile)

# Gets all the nodes of the graph (labels)
def getLabels(G, toDict):
    # List of nodes (the label)
    nodes = nx.nodes(G)

    for node in nodes:
        # print(node)
        print(nx.info(G))
        # print(f"\t{nx.get_edge_attributes(G, node)}")
    
    return 0

    if(toDict):
        retDict = dict()
        for i in range(len(nodes)):
            retDict[i] = nodes[i]
        return retDict

    return nodes

# Gets the number of nodes in a Graph
num_of_nodes = lambda G: nx.number_of_nodes(G)

# Gets the number of edges in a Graph
num_of_edges = lambda G: nx.number_of_edges(G)

# Parses the API
def parse_api_list(G):
    retLst = list()
    externals = nx.get_node_attributes(G, 'external')
    nodes = G.nodes()
    for node in nodes:
        # print(f"{node}\n\texternal:{externals[node]}")
        if(externals[node]):
            api_label = getSearchableLabel(node)
            retLst.append(api_label)
    
    return retLst



def getSearchableLabel(label):
    label = label[1:].split(";")[0]
    label = label.replace("/", ".")
    return label

# def getLabelFromCFG(labelsList):
#     retLst = list()

#     for label in labelsList:
#         getLabel = label[1:].split(";")[0]
#         getLabel = getLabel.replace("/",".")
#         retLst.append(getLabel)

def main(file):
    G = nx.read_gml(file, label='label')
    lst = parse_api_list(G)
    return lst


if __name__ == "__main__":

    file = "test/1-copy.gml"

    # fixGML(file)

    # G = nx.read_gml(file, label='id')

    lst = main(file)

    print(lst)

    # d.pr(G)

    # nodes = G.nodes()

    # lstLabel = list()

    # externals = nx.get_node_attributes(G, 'external')
    # # for key in externals:
    # #     print(f"\t{key}=>{externals[key]}")

    # for node in G.nodes():
    #     external_attr = externals[node]
    #     # print(external_attr)
    #     # print(type(externals[node]))
    #     # print(type(bool(externals[node])))
    #     if(external_attr == "True"):
    #         # print(f"{node}")
    #         # lstLabel.append(node)
    #         label = getSearchableLabel(node)
    #         print(label)

    # labels = getLabelFromCFG(lstLabel)

    # print(labels)
    # pAPI = parse_api_list(G)
    # externals = nx.get_node_attributes(G, 'external')
    # entrypoints = nx.get_node_attributes(G, 'entrypoint')

    # d.pr(externals)
    # d.pr(entrypoints)

    # for id, label in enumerate(nodes):
    #     prl = f"{id} -> {label} " # {external}"
    #     print(prl)
        # print(nodes['external'])

    # H = nx.read_gml(file, label='f"{id}={label}={external}"')
    # noes = H.nodes
    # print(noes)

    # node = nx.info(G)
    # d.pr(node)

    # nodes = getLabels(G, False)
    # d.pr(nodes)

    # num_of_edges(G)


    pass